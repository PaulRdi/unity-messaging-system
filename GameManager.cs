﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Barebones unsafe GameManager class to make the Messaging System work.
/// </summary>
public class GameManager : MonoBehaviour
{
    public static GameManager instance
    {
        get
        {
            return _instance;
        }
    }

    static GameManager _instance;

    private void Awake()
    {
        _instance = this;
        _instance.messageHub = new MessageHub();
    }

    public void Update()
    {
        while (messageHub.messageQueue.Count > 0)
        {
            messageHub._PopQueue();
        }
    }

    public MessageHub messageHub;
}